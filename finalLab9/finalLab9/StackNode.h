#pragma once
template<class T> class StackNode
{
public:
	StackNode();

	void setNextPtr(StackNode<T> * nextNode);
	StackNode * getNextPtr();

	void setData(StackNode<T> &newData);
	T getData();
private:
	StackNode * pNext;
	T data;
};
template<class T>
StackNode<T>::StackNode() {
	pNext = nullptr;
}

template<class T>
StackNode<T> * StackNode<T>::getNextPtr() {
	return pNext;
}
template<class T>
void StackNode<T>::setData(StackNode<T> &newData) {
	data = newData;
}
template<class T>
T StackNode<T>::getData() {
	return data;
}
template<class T>
void StackNode<T>::setNextPtr(StackNode<T> * nextNode) {
	pNext = nextNode;
}