#pragma once
#include "StackNode.h"
template<class T>class Stack
{
private:
	StackNode<T> * makeNode(T data);
	StackNode<T> * root;
	void DestructorHelper(StackNode<T> * curNode); // recursive
	void CopyHelper(StackNode<T> * curNode); //recursive
public:
	T Pop();
	Stack();
	void PrintStack();
	void Push(T data);
	Stack(Stack<T> const &original);
	~Stack();
};
template<class T>
Stack<T>::Stack() {
	root = nullptr;
}
template<class T>
Stack<T>::Stack(Stack<T> const &original) {
	if (original == nullptr) {
		return;
	}
	CopyHelper(StackNode<T> * curNode);
}
template<class T>
void Stack<T>::CopyHelper(StackNode<T> * curNode) {
	if (curNode == nullptr) {
		return;
	}
	Push(curNode->getData());
	CopyHelper(curNode->getNextPtr());
}

template<class T>
void Stack<T>::PrintStack() {
	StackNode<T> * temp = root;
	while (temp->getNextPtr() != nullptr) {
		cout << temp->getData() << endl;
		temp = temp->getNextPtr();
	}
}

template<class T>
void Stack<T>::Push(T data) {
	if (root == nullptr) {
		root = makeNode(data);
	}
	else {
		StackNode * temp = root;
		root = makeNode(data);
		root->setNextPtr(temp);
	}
}
template<class T>
StackNode<T> * Stack<T>::makeNode(T data) {
	StackNode * temp = new StackNode(data);
	temp->setNextPtr(root);
	return temp;
}

template<class T>
T Stack<T>::Pop() {
	T temp;
	root = root->getNextPtr();
}



